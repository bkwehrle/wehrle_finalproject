﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableText : MonoBehaviour
{
    float timer = 0f;
    Transform playerCam;
    void Start()
    {
        playerCam = GameObject.FindGameObjectWithTag("PlayerCamera").transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCam != null){
            transform.rotation = playerCam.transform.rotation;
            //transform.LookAt(2*transform.position - playerCamTransform.position,Vector3.up);
        }
        
    }
}
