﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class RandomShipSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ship;
    public float spawntime;
    ShipController shipController;
    Object[] shipPrefabs;
    GameObject shipPrefab;
    float timer;
    
    void Awake()
    {
        shipPrefabs = Resources.LoadAll("Ships", typeof(GameObject));
        shipPrefab = (GameObject)shipPrefabs[Random.Range(0, shipPrefabs.Length)];
        shipController = ship.GetComponent<ShipController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (shipController.flying)
        {
            timer += Time.deltaTime;
            if (timer > spawntime)
            {
                SpawnEncounter();
                timer = 0f;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape)){
            SceneManager.LoadScene(0);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        
    }
    void SpawnEncounter()
    {
        int temp = Random.Range(0, 3);
        if (temp == 0)
        {
            GameObject a = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
        } else if (temp == 1)
        {
            GameObject a = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
            GameObject b = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
        } else
        {
            GameObject a = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
            GameObject b = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
            GameObject x = Instantiate(shipPrefab, new Vector3(ship.transform.position.x + Random.Range(-200f, 200f), ship.transform.position.y + 70f, ship.transform.position.z + Random.Range(-200f, 200f)), Quaternion.identity);
        }
        

    }
}
