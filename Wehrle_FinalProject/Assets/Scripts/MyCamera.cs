﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject target;
    public float xSpeed = 3.5f;
    float sensitivity = 17f;

    float minFov = 35f;
    float maxFov = 100f;
    Vector3 originalTransformPos;
    float camLerp;

    Camera cam;

    Quaternion originalRot;

    void Awake()
    {
        cam = GetComponent<Camera>();
        originalTransformPos = transform.localPosition;
        originalRot = transform.localRotation;
    }   


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(target.transform.position, transform.up, Input.GetAxis("Mouse X") * xSpeed);
            transform.RotateAround(target.transform.position, transform.right, -Input.GetAxis("Mouse Y") * xSpeed);
            camLerp = 0f;
        }
        else
        {
            if (camLerp < 1f)
            {
                camLerp = Mathf.MoveTowards(camLerp, 1f, Time.deltaTime*.4f);

                transform.localPosition = Vector3.Lerp(transform.localPosition, originalTransformPos, camLerp);
                transform.localRotation = Quaternion.Lerp(transform.localRotation, originalRot, camLerp);
            }
        }
        //ZOOM

        float fov = cam.fieldOfView;
        fov += Input.GetAxis("Mouse ScrollWheel") * -sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        cam.fieldOfView = fov;

    }
}
