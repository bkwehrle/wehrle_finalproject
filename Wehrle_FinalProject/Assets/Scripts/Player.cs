﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    //GameObject[] planet;
    GameObject currentPlanet;
    GameObject PlayerPlaceholder;
    ItemAccountant itemAccountant;

    public float speed = 4;
    public float JumpHeight = 1.2f;

    float gravity = 100f;
    bool OnGround = false;

    Rigidbody rb;
    float distanceToGround;
    Vector3 GroundNormal;
    bool jumping = false;
    BoxCollider shipBoxCollider;
    string[] items = new string[4]{"RedItem","BlueItem","GreenItem","YellowItem"};
    AudioSource aud;
    public AudioClip jump;
    public AudioClip powerup;
    

    void Awake()
    {
        aud = GetComponent<AudioSource>();
        itemAccountant = GameObject.FindGameObjectWithTag("ItemAccountant").GetComponent<ItemAccountant>();
        PlayerPlaceholder = GameObject.FindGameObjectWithTag("PlayerPlaceHolder");
        shipBoxCollider = GameObject.FindGameObjectWithTag("Ship").GetComponent<BoxCollider>();
        FindCurrentPlanet();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        SearchPlayerOnAwake();

        //NEW APPROACH DISABLE THE COLLIDER WHEN THE YOUR ON THE SHIP
        //IF THERES A WILL THERES A WAY.
        //Physics.IgnoreCollision(GameObject.FindGameObjectWithTag("Ship").GetComponent<Collider>(), GetComponent<Collider>());
    }

    // Update is called once per frame
    void FixedUpdate(){
        
            
        if (jumping){
            rb.AddForce(transform.up * 40000* JumpHeight * Time.fixedDeltaTime);
        }
    }
    
    void Update()
    {

        //Movement

        float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        transform.Translate(x, 0, z);

        //Local Rotation

        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, 150 * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0, -150 * Time.deltaTime, 0);
        }

        //Jump

        if (Input.GetKeyDown(KeyCode.Space)&& OnGround)
        {
            aud.clip = jump;
            aud.Play();
            jumping = true;
        } else
        {
            jumping = false;
        }


        //Grounded

        RaycastHit hit = new RaycastHit();
        shipBoxCollider.enabled = false;
        if (Physics.Raycast(transform.position,-transform.up,out hit, 10))
        {
            distanceToGround = hit.distance;
            GroundNormal = hit.normal;
            //print(distanceToGround);
            if (distanceToGround <= 0.5f)
            {
                OnGround = true;
            } else
            {
                OnGround = false;
            }
        }
        shipBoxCollider.enabled = true;

        //print(OnGround);

        //Gravity and rotation

        Vector3 gravityDirection = (transform.position - currentPlanet.transform.position).normalized;

        if(OnGround == false)
        {
            rb.AddForce(gravityDirection * -gravity);
        }

        //

        Quaternion toRotation = Quaternion.FromToRotation(transform.up, GroundNormal) * transform.rotation;
        transform.rotation = toRotation;
        
    }

    void FindCurrentPlanet(){
        float longest = 0;
        float tmpDistance;
        foreach(GameObject planet in GameObject.FindGameObjectsWithTag("Planet")){
            tmpDistance = Mathf.Abs(Vector3.Distance(transform.position, planet.transform.position));
            if (tmpDistance>longest){
                longest = tmpDistance;
                currentPlanet = planet;
            }
        }
    }

    
    private void OnTriggerEnter(Collider collision)
    {   
        //print(collision.gameObject.name);
        if (collision.gameObject.tag == "RedItem"||collision.gameObject.tag == "BlueItem"||collision.gameObject.tag == "GreenItem"||collision.gameObject.tag == "YellowItem")
        {
            //print("test");
            SearchPlayer(collision.gameObject.tag);
            collision.gameObject.SetActive(false);
            aud.clip = powerup;
            aud.Play();
        }
        
        
        else if (collision.transform != currentPlanet.transform && collision.gameObject.tag != "Ship") {
 
            currentPlanet = collision.transform.gameObject;
 
            Vector3 gravDirection = (transform.position - currentPlanet.transform.position).normalized;
 
            Quaternion toRotation = Quaternion.FromToRotation(transform.up, gravDirection) * transform.rotation;
            transform.rotation = toRotation;
 
            rb.velocity = Vector3.zero;
            rb.AddForce(gravDirection * gravity);
 
 
            PlayerPlaceholder.GetComponent<PlayerPlaceholder>().NewPlanet(currentPlanet);
 
        } 
         
    }
    //searches player for the which new item to add on their head.
    void SearchPlayer(string s){
        foreach (Transform child in transform)
        {
            if (child.gameObject.tag == s) { 
                child.gameObject.SetActive(true);
                itemAccountant.ItemFound(s);
            }
        }
    }
    void SearchPlayerOnAwake(){
        
        foreach (Transform child in transform)
        {
            for (int i = 0; i < 4; i ++){
                if (itemAccountant.ItemThere(items[i])) { 
                    if (child.gameObject.tag == items[i]) { 
                        child.gameObject.SetActive(true);
                    }
                }
            }
            
        }
    }
}
