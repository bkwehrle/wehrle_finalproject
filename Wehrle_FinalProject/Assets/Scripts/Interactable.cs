﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Interactable : MonoBehaviour
{
    // Start is called before the first frame update
    
    GameObject playerObj;
    //public string interactableTagName;
    MeshRenderer meshRenderer;
    public GameObject interactableText;
    GameObject g;
    void Start()
    {   
        if(interactableText != null){
           g = Instantiate(interactableText,transform.position,Quaternion.identity);
           meshRenderer = g.GetComponent<MeshRenderer>();
        }
        playerObj = GameObject.FindGameObjectWithTag("Player");
        //interactableText.enabled = false;
    }
    public void FindPlayer(){
        playerObj = GameObject.FindGameObjectWithTag("Player");
    }
    
    public bool CurrentlyInteractable(){
        if (meshRenderer != null){
            if (meshRenderer.enabled == true){
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }
    public void Interaction(){
        Destroy(g);
    }
    public void ResetInteration(){
        if(interactableText != null){
           g = Instantiate(interactableText,transform.position,Quaternion.identity);
           meshRenderer = g.GetComponent<MeshRenderer>();
        }
    }
    void Update()
    {
        if (playerObj != null && transform != null && meshRenderer != null){
            if (Mathf.Abs(Vector3.Distance(playerObj.transform.position,transform.position))< 4.5f){
                meshRenderer.enabled = true;
            } else {
                meshRenderer.enabled = false;
            }
        } else {
            if (meshRenderer != null){
                meshRenderer.enabled = false;
            }
        }
        
    }
}
