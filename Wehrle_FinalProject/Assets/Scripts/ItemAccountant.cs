﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ItemAccountant : MonoBehaviour
{
    public bool GameisWon;
    void Start() {
        GameisWon = false;
    }
    public static ItemAccountant _instance;
    public static ItemAccountant Instance
    {
        get {
            return _instance;
        }
    }
    bool RedItem = false;
    bool GreenItem = false;
    bool BlueItem = false;
    bool YellowItem = false;

    public void ItemFound(string s){
        if(s == "RedItem"){
            RedItem = true;
        }
        if(s == "BlueItem"){
            BlueItem = true;
        }
        if(s == "GreenItem"){
            GreenItem = true;
        }
        if(s == "YellowItem"){
            YellowItem = true;
        }
        if (RedItem && BlueItem && GreenItem&& YellowItem){
            StartCoroutine(GameWon());
        }
    }
    public bool ItemThere(string s){
        if(s == "RedItem"){
            return RedItem;
        }
        if(s == "BlueItem"){
            return BlueItem;
        }
        if(s == "GreenItem"){
            return GreenItem;
        }
        if(s == "YellowItem"){
            return YellowItem;
        }
        return false;
    }
    IEnumerator GameWon(){
        yield return new WaitForSeconds(1f);
        GameTimer timer = GameObject.FindGameObjectWithTag("Time").GetComponent<GameTimer>();
        PlayerPrefs.SetString("score",Mathf.Round(timer.timer).ToString());
        GameisWon = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(2);
    }
}
