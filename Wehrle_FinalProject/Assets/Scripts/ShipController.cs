﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ShipController : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    float HP = 5;
    Rigidbody rb;
    public GameObject ShipCamObject;
    public GameObject Particles;
    public GameObject LaserPoint;
    public GameObject Bullet;
    Camera ShipCam;
    public bool flying = false;
    Rigidbody bulletRb;
    BoxCollider boxCollider;
    Interactable myInteract;
    Vector3 LaunchUp;
    Vector3 LaunchRight;
    public GameObject Player;
    GameObject PlayerGameObject;
    //Vector3 landingPlace;
    public GameObject Explosion;
    public GameObject HealthObj;
    public AudioSource EnginePlayer;
    public AudioSource ShootPlayer;
    public AudioClip shootSound;
    public AudioClip preExplosion;
    public AudioClip explosion;
    float healthTimer;
    bool ending= false;
    Text healthText;
    
        

        void Awake()
    {
        PlayerGameObject = GameObject.FindGameObjectWithTag("Player");
        healthText = HealthObj.GetComponent<Text>();
        myInteract = GetComponent<Interactable>();
        LaunchUp = transform.up;
        LaunchRight = transform.right;
        ShipCam = ShipCamObject.GetComponent<Camera>();
        rb = GetComponent<Rigidbody>();
        //boxCollider = GetComponent<BoxCollider>();
        //boxCollider.enabled = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    void Start(){
        //Interactable myInteract = new Interactable();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (flying)
        {
            transform.Rotate(LaunchUp, -Input.GetAxis("Mouse X") * 1f);
            transform.Rotate(LaunchRight, -Input.GetAxis("Mouse Y") * 1f);
            //print(LaunchUp);
            //print(LaunchRight);

            //Vector3 euler = transform.rotation.eulerAngles;
            //euler.z = 0f;
            //transform.rotation = Quaternion.Euler(euler);


            /*
            if (Input.GetKey(KeyCode.W))
            {

                transform.Rotate(-25 * Time.deltaTime, 0, 0);
                //rb.AddForce(transform.forward * Time.deltaTime * 10f, ForceMode.VelocityChange);
            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddForce(-transform.forward * Time.deltaTime * 6f, ForceMode.VelocityChange);
            }
            if (Input.GetKey(KeyCode.A))
            {

                rb.AddTorque(-transform.up * Time.deltaTime * .4f, ForceMode.VelocityChange);
            }
            if (Input.GetKey(KeyCode.D))
            {

                rb.AddTorque(transform.up * Time.deltaTime * .4f, ForceMode.VelocityChange);
            }
            if (Input.GetKey(KeyCode.E))
            {
                transform.Rotate(25 * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.Q))
            {
            }
            if (Input.GetKey(KeyCode.F))
            {
                
                rb.freezeRotation = rb.freezeRotation == true ? false: true;
                rb.velocity = Vector3.zero;
            }
            */
            if (Input.GetKey(KeyCode.Space))
            {

                rb.AddForce(transform.forward * Time.deltaTime * speed, ForceMode.VelocityChange);
                Particles.SetActive(true);
                EnginePlayer.mute = false;
            } else {
                Particles.SetActive(false);
                EnginePlayer.mute = true;
            }
            if (Input.GetKeyDown(KeyCode.Mouse0)){
                //LaserPoint
                //GameObject b = Instantiate(Bullet,new Vector3 (transform.localPosition.x,transform.localPosition.y,transform.localPosition.z+5),transform.rotation);
                GameObject b = Instantiate(Bullet,LaserPoint.transform.position,Quaternion.identity);
                bulletRb = b.GetComponent<Rigidbody>();
                bulletRb.AddForce(transform.forward*150f,ForceMode.Impulse);
                ShootPlayer.clip = shootSound;
                ShootPlayer.Play();
                Destroy(b,1.4f);
            }

        } else
        {
            rb.velocity = Vector3.zero;
            healthTimer += Time.deltaTime;
            if (healthTimer > 3f && ending== false){
                healthTimer = 0f;
                if (HP < 5){
                    HP += 1;
                    healthText.text = "HP: "+HP.ToString()+"/5";
                }
            }
        }
        if (myInteract.CurrentlyInteractable()){
            if (Input.GetKeyDown(KeyCode.F)){
                TakeOff();
            }
        }
        
    }

    public void TakeOff()
    {
        //print("TAKEOFF");
        Destroy(PlayerGameObject);
        myInteract.Interaction();
        flying = true;
        //boxCollider.enabled = true;
        ShipCam.enabled = true;
    }
    public void Landing()
    {
        flying = false;
        Particles.SetActive(false);
        EnginePlayer.mute = true;
        ShipCam.enabled = false;
        PlayerGameObject = Instantiate(Player,new Vector3 ( transform.position.x +1f,transform.position.y +1f,transform.position.z +1f),Quaternion.identity);
        myInteract.FindPlayer();
        
        myInteract.ResetInteration();
        //boxCollider.enabled = false;
        
        //make Player
    }
    
        

    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "Planet"&&flying)
        {
            Landing();
        }
    }
    void OnTriggerEnter(Collider other){
        if(other.gameObject.tag == "EnemyBullet"){
            if (flying){
                GameObject g = Instantiate(Explosion,transform.position,Quaternion.identity);
                Destroy(g, 4f);
                HP -= 1;
                if (HP <= 0)
                {
                    StartCoroutine(EndGame());
                } else
                {
                    ShootPlayer.clip = preExplosion;
                    ShootPlayer.Play();
                    healthText.text = "HP: "+HP.ToString() + "/5";
                }
            }
        }
    }
    IEnumerator EndGame(){
        flying = false;
        ending = true;
        ShootPlayer.clip = preExplosion;
        ShootPlayer.Play();
        rb.constraints = RigidbodyConstraints.None;
        rb.AddTorque(Random.insideUnitSphere*.3f,ForceMode.Impulse); 
        yield return new WaitForSeconds(2f);
        GameObject c = Instantiate(Explosion,transform.position,Quaternion.identity);
        Destroy(c, 4f);
        ShootPlayer.clip = explosion;
        ShootPlayer.Play();
        yield return new WaitForSeconds(3f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(0);
    }
}
