﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snipper : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = .5f;
    GameObject Ship;
    Rigidbody rb;
    BoxCollider boxCollider;
    ShipController shipController;
    public MeshRenderer meshRenderer;
    bool firing = false;
    bool alive = true;
    public GameObject Explosion;
    public GameObject enemyBullet;
    public GameObject firePoint;
    AudioSource audioSource;
    public AudioClip explosion;
    public AudioClip preexplosion;
    public AudioClip shoot;
    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        Ship = GameObject.FindGameObjectWithTag("Ship");
        shipController = Ship.GetComponent<ShipController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (shipController.flying && (alive == true)&&(Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position))<600f)){
            transform.LookAt(Ship.transform);
            if ((Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position))>15f) ){
                rb.AddForce((Ship.transform.position - transform.position)*Time.fixedDeltaTime*speed,ForceMode.VelocityChange);
            }
        }
        else if (alive == true&& (Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position)) > 40f))
        {
            rb.AddForce((transform.forward) * Time.fixedDeltaTime * speed * 6f, ForceMode.VelocityChange);
        }

        //Quaternion toRotation = Quaternion.LookRotation(transform.forward,Ship.transform.position);
        //transform.rotation = Quaternion.Lerp(transform.rotation,toRotation, speed * 100f* Time.fixedDeltaTime);
    }
    void Update(){
        if (Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position))<25f && (firing == false) && (alive == true)&&(shipController.flying)){
            StartCoroutine(Fire());
        }
        
    }
    IEnumerator Fire(){
        
        firing = true;

        Vector3 directionToFire = Ship.transform.position - transform.position;
        GameObject g = Instantiate(enemyBullet,firePoint.transform.position, Quaternion.identity);
        Rigidbody bulletRb = g.GetComponent<Rigidbody>();
        bulletRb.AddForce(directionToFire*3f,ForceMode.Impulse);
        audioSource.clip = shoot;
        audioSource.Play();
        Destroy(g,1.4f);

        yield return new WaitForSeconds(3.5f);
        firing = false;
    }
    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Bullet"){
            if (alive){
                StartCoroutine(Die());
            }
            
        }
    }
    IEnumerator Die(){
        alive = false;
        audioSource.clip = preexplosion;
        audioSource.Play();
        rb.constraints = RigidbodyConstraints.None;
        rb.AddTorque(Random.insideUnitSphere*10f,ForceMode.Impulse); 
        yield return new WaitForSeconds(.8f);
        //GameObject c = Instantiate(Explosion,transform.position,Quaternion.identity);
        yield return new WaitForSeconds(.7f);
        
        GameObject g = Instantiate(Explosion,transform.position,Quaternion.identity);
        Destroy(g, 4f);
        audioSource.clip = explosion;
        audioSource.Play();
        meshRenderer.enabled = false;
        boxCollider.enabled = false;
        Destroy(gameObject,1.2f);
    }
}
