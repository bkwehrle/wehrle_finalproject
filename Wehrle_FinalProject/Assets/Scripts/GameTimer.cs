﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameTimer : MonoBehaviour
{
    public float timer = 0f;
    Text timerText;
    ItemAccountant itemAccountant;
    bool RunTimer = true;
    void Awake()
    {
        timerText = GetComponent<Text>();
        itemAccountant = GameObject.FindGameObjectWithTag("ItemAccountant").GetComponent<ItemAccountant>();
    }

    void Update()
    {
        if (RunTimer == true){
            //print(itemAccountant.GameisWon);
            timer += Time.deltaTime;
            timerText.text = "Time: " + Mathf.Round(timer).ToString();
            if (itemAccountant.GameisWon){
                RunTimer = false;
            }
        }
        
    }
}
