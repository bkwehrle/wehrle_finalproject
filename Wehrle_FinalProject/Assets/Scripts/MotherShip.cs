﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherShip : MonoBehaviour
{
    float speed = .3f;
    int hp = 3;
    GameObject Ship;
    Rigidbody rb;
    BoxCollider boxCollider;
    ShipController shipController;
    public MeshRenderer meshRenderer;
    public MeshRenderer meshRenderer1;
    public MeshRenderer meshRenderer2;
    bool spawning = false;
    bool alive = true;
    public GameObject Explosion;
    public GameObject snipper;
    public GameObject spawnPoint;
    AudioSource audioSource;
    public AudioClip explosion;
    public AudioClip preexplosion;
    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        Ship = GameObject.FindGameObjectWithTag("Ship");
        shipController = Ship.GetComponent<ShipController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (shipController.flying && (alive == true) && (Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position)) < 600f))
        {
            transform.LookAt(Ship.transform);
            if ((Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position)) > 95f))
            {
                rb.AddForce((Ship.transform.position - transform.position) * Time.fixedDeltaTime * speed, ForceMode.VelocityChange);
            }
        }
        else if (alive == true && (Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position)) > 40f))
        {
            rb.AddForce((transform.forward) * Time.fixedDeltaTime * speed * 6f, ForceMode.VelocityChange);
        }
        
        //Quaternion toRotation = Quaternion.LookRotation(transform.forward,Ship.transform.position);
        //transform.rotation = Quaternion.Lerp(transform.rotation,toRotation, speed * 100f* Time.fixedDeltaTime);
    }
    void Update(){
        if (Mathf.Abs(Vector3.Distance(transform.position, Ship.transform.position))<105f && (spawning == false) && (alive == true)&&(shipController.flying)){
            StartCoroutine(SpawnShip());
        }
    }
    IEnumerator SpawnShip(){
        spawning = true;
        Vector3 directionToFire = Ship.transform.position - transform.position;
        GameObject g = Instantiate(snipper,spawnPoint.transform.position, Quaternion.identity);
        //Destroy(g,1.4f);
        yield return new WaitForSeconds(12.5f);
        spawning = false;
    }
    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Bullet"){
            if (alive){
                StartCoroutine(Hit());
            }
            
        }
    }
    IEnumerator Hit(){
        
        GameObject g = Instantiate(Explosion,transform.position,Quaternion.identity);
        Destroy(g, 4f);
        audioSource.clip = preexplosion;
        audioSource.Play();
        hp--;
        if (hp <= 0){
            alive = false;
            rb.constraints = RigidbodyConstraints.None;
            rb.AddTorque(Random.insideUnitSphere*10f,ForceMode.Impulse); 
            yield return new WaitForSeconds(.8f);
            //GameObject c = Instantiate(Explosion,transform.position,Quaternion.identity);
            yield return new WaitForSeconds(.7f);
            
            GameObject f = Instantiate(Explosion,transform.position,Quaternion.identity);
            f.transform.localScale = new Vector3(3.2f, 3.2f, 3.2f);
            Destroy(f, 4f);
            audioSource.clip = explosion;
            audioSource.Play();
            meshRenderer.enabled = false;
            meshRenderer1.enabled = false;
            meshRenderer2.enabled = false;
            boxCollider.enabled = false;
            Destroy(gameObject,1.2f);
        }
    }
}
