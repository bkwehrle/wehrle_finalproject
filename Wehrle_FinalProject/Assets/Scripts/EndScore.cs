﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScore : MonoBehaviour
{
    Text scoreText;
    void Start()
    {
        scoreText = GetComponent<Text>();
        scoreText.text = "Your Time was " +PlayerPrefs.GetString("score")+"!";
    }

    
}
